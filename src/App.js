import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import React, { Component } from 'react';
import history from './history';
import './App.css';

import 'bootstrap/dist/css/bootstrap.min.css';

import Todo from './components/todo/Todo';

class App extends Component {
  render () {
      return (
        <Router history={history}>
            <div className="App">
                <Switch>
                    <Route exact path="/" component={Todo} />
                </Switch>
            </div>
        </Router>
      );
  }
}

export default App;
