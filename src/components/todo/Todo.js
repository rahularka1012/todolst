import React, { Component } from "react";
import './Todo.css';

class Todo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            todoData: [
                {
                    id: 1,
                    title: "What is Lorem Ipsum?",
                    text: "Lorem Ipsum is simply dummy text of the printing and typesetting industry."
                }, 
                {   
                    id: 2,
                    title: "Why do we use it?",
                    text: "It is a long established fact that a reader will be distracted by the readable......."
                }, 
                {   
                    id: 3,
                    title: "Where does it come from?", 
                    text:" Contrary to popular belief, Lorem Ipsum is not simply random text."
                }
            ],
            todoId: "",
            title: "",
            text: "",
            submitBtnTxt: "Submit",
            errCls: "alert-success",
            errMsg: ""
        };
    }

    componentDidMount() {
        //
    };

    handleChange(e) {
        this.setState({ [e.target.name]: e.target.value });        
    }

    handleSubmit(e){
        e.preventDefault();

        let todoData = this.state.todoData;

        var keys = Object.keys(todoData);
        var last = keys[keys.length-1];
        
        let todoId = this.state.todoId;
        let id = (todoId !== "") ? todoId : (todoData[last].id+1);

        let postTodo = {
            id: id,
            title: this.state.title,
            text: this.state.text
        }

        if(todoId !== "")
        {
            let index = todoData.findIndex(a => a.id === todoId);

            if (index === -1) return;
            todoData[index] = postTodo;
            
            this.setState({todoData: todoData, todoId: "", title: "", text: "", errCls: "alert-success", errMsg: "Todo updated successfully.", submitBtnTxt: "Submit"});
        }else{
            todoData.push(postTodo);
            
            this.setState({ todoId: "", title: "", text: "", errCls: "alert-success", errMsg: "Todo created successfully." }); 
        }   
    };

    getTodoData(todoId, e)
    {
        let todoData = this.state.todoData;
        let obj = todoData.find(e => e.id === todoId);

        this.setState({ todoId: todoId, title: obj.title, text: obj.text, submitBtnTxt: "Update" });
    }

    deleteTodo(todoId, e)
    {
        let newTodoData = this.state.todoData;
        let index = newTodoData.findIndex(a => a.id === todoId);

        if (index === -1) return;
        newTodoData.splice(index, 1);
        
        this.setState({todoData: newTodoData});
    }

    closeAlert()
    {
        this.setState({errMsg:""});
    }

    render() {
        let { todoData, title, text, submitBtnTxt, errCls, errMsg } = this.state;

        return (
            <section className="content container py-3">
                <div className="row">
                    <div className="col-6">
                        <h3> TODO App</h3>
                    </div>
                    <div className="col-6 text-right">
                        <button type="button" className="btn btn-secondary"> ToDo List </button>
                    </div>
                    <div className="col-12">
                        <hr />
                    </div>
                </div>

                <div className="row">
                    <div className="col-md-10 m-auto">
                        { errMsg ? 
                            <div className={'alert ' + errCls}>
                                <span className="closebtn" onClick={this.closeAlert.bind(this)}>&times;</span> 
                                {errMsg}
                            </div>
                            : ''
                        }
                        <div className="todo-head">
                            <h3>Note Details</h3>
                            <hr />
                        </div>

                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <div className="form-group">
                                <label for="title">Title</label>
                                <input type="text" className="form-control" id="title" name="title" placeholder="Enter title" onChange={this.handleChange.bind(this)} value={title} maxLength="50" required/>
                            </div>
                            <div className="form-group">
                                <label for="text">Text</label>
                                <input type="text" className="form-control" id="text" name="text" placeholder="Enter text" onChange={this.handleChange.bind(this)} value={text} maxLength="150" required/>
                            </div>
                            
                            <div className="form-group form-foot">
                                <button type="submit" className="btn btn-primary">{submitBtnTxt}</button>
                            </div>
                        </form>
                    </div>
                    <div className="col-12">
                        <hr />
                    </div>
                    <div className="col-md-10 m-auto">
                        <div className="todolist-block row">
                            <div className="col-12">
                                <div className="todo-head">
                                    <h3>TODO List</h3>
                                    <hr />
                                </div>
                            </div>
                            {(todoData.length > 0) ?
                                todoData.map(todo => (
                                    <div className="col-sm-4 mb-4">
                                        <div className="todo-item border rounded p-3">
                                            <h4>{todo.title}</h4>
                                            <p>{todo.text}</p>

                                            <div className="todo-foot d-flex align-items-center justify-content-around">
                                                <button type="button" className="btn btn-info" dataid={todo.id} onClick={this.getTodoData.bind(this, todo.id)}>Edit</button>
                                                <button type="button" className="btn btn-danger" dataid={todo.id} onClick={this.deleteTodo.bind(this, todo.id)}>Delete</button>
                                            </div>
                                        </div>
                                    </div>
                                ))
                                : 
                                <div className="col-sm-12">
                                    <span>No todo found!</span>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </section>            
        );
    }
}

export default Todo;